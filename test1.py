

#!/usr/bin/env python3.6

""" Ideas:

1: https://stackoverflow.com/questions/17128105/tkinter-binding-event-from-controller#17128431

2: https://sukhbinder.wordpress.com/2014/12/25/an-example-of-model-view-controller-design-pattern-with-tkinter-python/

"""

import tkinter as Tk

class Model():

    def __init__(self):

        self.res = 1

    def do(self):

        return self.res

class View():

    def __init__(self, master, controller):

        self.controller = controller

        self.frame = Tk.Frame(master)

        self.frame.pack()

        self.viewPanel = ViewPanel(master, controller);

class ViewPanel():

    def __init__(self, root, controller):

        self.controller = controller

        self.framePanel = Tk.Frame(root)

        self.framePanel.pack()

        self.label = Tk.Label(self.framePanel, text="Result:")

        self.label.pack()

        self.entry = Tk.Entry(self.framePanel)

        self.entry.pack()

        self.btn = Tk.Button(self.framePanel, text="Get")

        self.btn.pack()

        # Event handlers passes events to controller

        self.btn.bind("<Button>", controller.btnHandler);

class Controller():

    def __init__(self):

        self.root = Tk.Tk()

        self.model = Model()

        # Pass to view links on root frame and controller object

        self.view = View(self.root, self)

        # Event handlers

        # 

        self.root.title("MVC example")

        #self.root.deiconify()

        self.root.mainloop()

    def btnHandler(self, event):

        # event.widget

        self.action()

    def action(self):

        # Pass data to view

        self.view.viewPanel.entry.insert(0, self.model.do())

if __name__ == '__main__':

    c = Controller()
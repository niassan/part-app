import tkinter as tk
from tkinter import *
# from pubsub import pub # massage passing between MVC


# window = Tk()
# window.title("Welcome to LikeGeeks app")
# window.mainloop()

class View:

    def __init__(self,parent):
        # Initialize variableas
        self.container = parent
        # self.master = parent
        return

    def setup(self):
        self.create_widgets()
        self.setup_layout()

    def create_widgets(self):
        print("create_widgets")
        

        # # Set Layout 
        frameTopL = Frame(self.container, width=512, height=192, background="blue")
        frameTopL.grid(row=0, column=0,sticky=W + E + N + S)
        # frameTopL.pack()
        frameTopR = Frame(self.container, width=512, height=192, background="Red")
        frameTopR.grid(row=0, column=1, sticky=W + E + N + S)
        # frameTopR.pack()
        frameBodyL = Frame(self.container, width=512, height=576, background="White")
        frameBodyL.grid(row=1, column=0)
        # frameBodyL.pack()
        frameBodyR = Frame(self.container, width=512, height=576, background="DarkOrange")
        frameBodyR.grid(row=1, column=1)
        # frameBodyR.pack()

        # Layout Top Left
        labelBarcode = Label(frameTopL, text = "Barcode") 
        labelBarcode.grid(row=0, column=0,rowspan=1)

        entryBarcode = Entry(frameTopL, background="pink")
        entryBarcode.grid(row=0, column=1,rowspan=1)

        labelModel = Label(frameTopL, text = "Model") 
        labelModel.grid(row=0, column=2,rowspan=1)

        entryModel = Entry(frameTopL, background="Green")
        entryModel.grid(row=0, column=3,rowspan=1)

        # Layout Top Right

        labelRadio = Label(frameTopR)
        labelRadio.grid(row=0, column=0,rowspan=1)

        radio1 = Radiobutton(labelRadio, 
              text="Auto",
            #   padx = 20, 
            #   variable=tk.IntVar(), 
              value=1).pack(padx=5, pady=10, side=tk.LEFT)
        radio2 = Radiobutton(labelRadio, 
              text="Manual",
            #   padx = 20, 
            #   variable=tk.IntVar(), 
              value=2).pack(padx=5, pady=10, side=tk.LEFT)

        # Layout Body Left


        # Layout Body Rigth
        

        

    def setup_layout(self):
        print("setup_layout")

    #     # self.columnconfigure(0, pad=2)
    #     # self.columnconfigure(1, pad=2)
        
    #     # # layout all of the main containers
    #     # self.container.grid_rowconfigure(1, weight=1)
    #     # self.container.grid_columnconfigure(0, weight=1)

# test
if __name__ == "__main__":
    print("running view")
    mainwin = tk.Tk()
    WIDTH = 1024
    HEIGHT = 768
    mainwin.geometry("%sx%s" % (WIDTH, HEIGHT))
    # mainwin.resizable(0, 0)
    mainwin.title("Open CV")
    view = View(mainwin)
    view.setup()
    mainwin.mainloop()



import numpy as np
import opencv-python as cv2
import math
from skimage import measure
import kiwisolver as kiwi
from matplotlib import pyplot as plt
import os, os.path
import warnings
from time import sleep
from tkinter import *
import tkinter.messagebox
import tkinter.filedialog
#from tkinter import filedialog
from PIL import Image
from PIL import ImageTk
import PIL
import glob
import time

#from picamera.array import PiRGBArray
#from picamera import PiCamera
#import RPi.GPIO as GPIO

warnings.filterwarnings("ignore")
image =cv2.imread("b2.jpg")
def TakePicture():
    global photo, cv_img
    cap = cv2.VideoCapture(0)
    ret, cv_img = cap.read()
    #dim = cv_img.shape
    #print(dim)
    photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(cv_img))
    canvas.create_image(0, 0, image=photo, anchor=tkinter.NW)

def SavePicture():
    global cv_img
    filename = tkinter.filedialog.asksaveasfilename()
    print("saving",filename)
    cv2.imwrite(filename,cv_img)

def CV2Picture():
    global photo,cv_img
    cv2.imshow("cv_img",cv_img)

def OpenPicture():
    global cv_img, photo
    filename = tkinter.filedialog.askopenfilename()
    print(filename)
    cv_img = cv2.imread(filename)
    dim = (640, 480)
    dim_img = cv2.resize(cv_img, dim, interpolation = cv2.INTER_AREA)
    photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(dim_img))
    canvas.create_image(0, 0, image=photo, anchor=tkinter.NW)

def SimImage(img1,img2,mode): #mode 1=SSIM 2=SimHistogram
    color = ('b','g','r')
    deltaSum = 0
    deltaMax = 0
    if mode == 1 :
        #print("ssim measurement")
        result = measure.compare_ssim(img1, img2, multichannel=True)
    if mode == 2 :
        #print("histogram pattern measurement")
        for i,col in enumerate(color):
            hist1 = cv2.calcHist([img1],[i],None,[256],[0,256])
            #print(img1.shape,f,img2.shape)
            hist2 = cv2.calcHist([img2],[i],None,[256],[0,256])
            result = measure.compare_ssim(hist1, hist2, multichannel=True)
    if mode == 3 :
        #print("delta histogram measurement")
        for i,col in enumerate(color):
            hist1 = cv2.calcHist([img1],[i],None,[256],[0,256])
            hist2 = cv2.calcHist([img1],[i],None,[256],[0,256])
            err = abs(hist1 - hist2)
            deltaSum = deltaSum + err
    if (np.linalg.norm(err) > deltaMax) :
        deltaMax = np.linalg.norm(err)
        result = np.linalg.norm(deltaSum)/1000000
        result = 1 - result
    if mode ==4:
        # BGR split histogram
        b,g,r = cv2.split(img)
        img = cv2.merge((r,g,b))
        result = 0
        return result
        mode = 1

def CheckPicture():
    global cv_img, photo, path,mode
    Aname = ["zzz.jpg"]
    Avalue = [0.0]
    valid_images = [".jpg"]
    print(path)
    for f in os.listdir(path):
        ext = os.path.splitext(f)[1]
        if ext.lower() not in valid_images:
            continue
            image2 = cv2.imread(f)
            #try:
            x= SimImage(cv_img,image2,mode)
            print(f,x)
            #except:
            # x = 0.0
            Aname.append(f)
            Avalue.append(x)
            print(Aname,Avalue)
            Asort = np.argsort(Avalue)
            Asort = np.flip(Asort)
            i= 0
            for i in range(0,1):
                j = Asort[i]
                print(Aname[j],Avalue[j])
                A1 = Aname[Asort[0]]
                A2 = Aname[Asort[1]]
        if (A1[0] == A2[0]):
            Vote="cannot determine"
        if A1[0] == "a":
            Vote = "A"
        if A1[0] == "b":
            Vote = "B"
        if A1[0] == "c":
            Vote = "C"
        if A1[0] == "d":
            Vote = "D"
            print(Vote)
        else:
            print("cannot determine")

def TestHist():
    global cv_img # photo, path,mode
    cap = cv2.VideoCapture(0)
    ret, cv_img = cap.read()
    cv_img = cv
    hist11 = cv2.calcHist(cv_img,[0],None,[256],[0,256])
    hist12 = cv2.calcHist(cv_img,[1],None,[256],[0,256])
    hist13 = cv2.calcHist(cv_img,[2],None,[256],[0,256])
    plt.plot(hist11)
    plt.plot(hist12)
    plt.plot(hist13)


plt.show()
#image1= TakePicture()
#image1 = cv2.imread('b1.png')
#image2 = cv2.imread('b2.jpg')
#path = "/home/pi/Desktop/TestCode/Cognex"
path = "D:/CNK2019/BrainData/PC_Vision"
window = tkinter.Tk()
window.title("COGNEX-1")
window.geometry("600x600")
btn_TakePict=tkinter.Button(window, text="TakePhoto", width=20,height=2, command=TakePicture)
btn_TakePict.place(x = 1, y = 490)
btn_SavePict=tkinter.Button(window, text="SavePhoto", width=20,height=2, command=SavePicture)
btn_SavePict.place(x = 150, y = 490)
btn_OpenPict=tkinter.Button(window, text="OpenPhoto", width=20,height=2, command=OpenPicture)
btn_OpenPict.place(x = 300, y = 490)
btn_4=tkinter.Button(window, text="CheckPart", width=20,height=2, command=CheckPicture)
btn_4.place(x = 450, y = 490)
#cv_img = cv2.imread("/home/pi/Desktop/TestCode/Cognex/PictData/zDWlogo.jpg")
cv_img = cv2.imread("D:/CNK2019/BrainData/PC_Vision/aa1.jpg")
scale_percent = 50 # percent of original size
width = int(cv_img.shape[1] * scale_percent / 100)
height = int(cv_img.shape[0] * scale_percent / 100)
dim = (width, height)
# resize image
cv_img = cv2.resize(cv_img, dim, interpolation = cv2.INTER_AREA)
cv_img = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)

#height, width, no_channels = cv_img.shape
#print(height, width)
width = 640
height = 480
canvas = tkinter.Canvas(window, width = width, height = height)
canvas.place(x = 1, y = 1)
photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(cv_img))
canvas.create_image(0, 0, image=photo, anchor=tkinter.NW)

window.mainloop()
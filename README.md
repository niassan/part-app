
#### Install pip For Project

```

pip3 install -r requirements.txt

```


### The module brings implementations of intensity transformation algorithms to adjust image contrast.

ทำให้ภาพชัดเชนขึ้น

https://docs.opencv.org/4.4.0/dc/dfe/group__intensity__transform.html#gaabdbfa8715c8c813f4c862986b1a0882


#### cv::linemod::Match Struct Reference
https://docs.opencv.org/4.4.0/d9/da0/structcv_1_1linemod_1_1Match.html#a35299ee9799423ba150fc699bb0e65dd


#### Canny Edge Detection
https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_canny/py_canny.html


#### Basic MVC example for TkInter
http://maximusin9.01sh.ru/2018/02/basic-mvc-example-for-tkinter



